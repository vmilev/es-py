import serial
import time
import sys

if len(sys.argv) < 2:
    print("Usage: python script.py <output_file_name>")
    sys.exit(1)

port = '/dev/ttyUSB0'  # Lets assume we are using linux 
baudrate = 115200
bytesize = serial.EIGHTBITS
parity = serial.PARITY_NONE
stopbits = serial.STOPBITS_ONE
timeout = 1 

# Duration for data collection 
collection_duration = 3600 

# Output file name
output_file_name = sys.argv[1]

# Open the serial port
ser = serial.Serial(port, baudrate=baudrate, bytesize=bytesize, parity=parity, stopbits=stopbits, timeout=timeout)

with open(output_file_name, 'w') as file:
    start_time = time.time() 
    while True:
    
        if time.time() - start_time > collection_duration:
            print("Data collection completed.")
            break

        if ser.in_waiting > 0:
            data = ser.read(ser.in_waiting).decode('utf-8')
            file.write(data)
            file.flush()
            # Print the data to the console for gitlab purpose
            print(data)

ser.close()
